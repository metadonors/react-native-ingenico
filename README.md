
# react-native-ingenico

## Getting started

`$ npm install react-native-ingenico --save`

### Mostly automatic installation

`$ react-native link react-native-ingenico`

### Manual installation


#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNIngenicoPackage;` to the imports at the top of the file
  - Add `new RNIngenicoPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-ingenico'
  	project(':react-native-ingenico').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-ingenico/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-ingenico')
  	```

## Usage
```javascript

import Ingenico from 'react-native-ingenico';

const startPayment = async() => {
  try {
    const res = await Ingenico.startPayment("10,50", "GP122345")
  } catch(e) {
    console.log('ERROR:', e)
  }
}

const startTTL = async() => {
  try {
    const res = await Ingenico.startTTL("10,50", "GP122345")
  } catch(e) {
    console.log('ERROR:', e)
  }
}

const startRefund = async() => {
  try {
    const res = await Ingenico.startRefund()
  } catch(e) {
    console.log('ERROR:', e)
  }
}

const startCNS = async() => {
  try {
    const res = await Ingenico.startCNS()
  } catch(e) {
    console.log('ERROR:', e)
  }
}


```

In case of right answer POS returns something like:

```
{
	acquirerId: "88105000001"
	additionalData: ""
	amount: "10.5"
	authorizationNumber: null
	cvm: "1"
	date: "3007191822"
	exp: "****"
	merchantId: "309109500006   "
	operationNumber: "000046"
	pan: "524490*******0178"
	stan: "000025"
	status: 0
	termId: "50000003"
}
```	
  