
package com.metadonors.ingenico;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.WritableMap;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import java.util.Map;


public class RNIngenicoModule extends ReactContextBaseJavaModule {
  private static final String TAG = RNIngenicoModule.class.getCanonicalName();

  private static final String INGENICO_NAMESPACE= "com.ingenico.pos.external";
  private static final String INGENICO_INTENT_PAYMENT = INGENICO_NAMESPACE + ".pay";
  private static final String INGENICO_INTENT_TTL= INGENICO_NAMESPACE + ".ttlextended";
  private static final String INGENICO_INTENT_CNS= INGENICO_NAMESPACE + ".cns";
  private static final String INGENICO_INTENT_REFUND= INGENICO_NAMESPACE + ".reversal";

  private static final String E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST";
  private static final String E_FAILED_TO_SHOW_INGENICO = "E_FAILED_TO_SHOW_INGENICO";

  private static final String INGENICO_TTL_CAUSE = "DF81060354544C";

  static final int REQUEST_CODE_PAY	= 42001;
  static final int REQUEST_CODE_REFUND = 42002;
  static final int REQUEST_CODE_TTLEXT	= 42004;
  static final int REQUEST_CODE_CNS	= 42005;

  static final int RESULT_OK = 0;
  static final int RESULT_POS_NOT_CONNECTED = 9;
  static final int RESULT_PAYMENT_DECLINED = 20;
  static final int RESULT_UNCAUGHT_EXCEPTION = 33;
  static final int RESULT_POS_NOT_INITIALIZED = 95;
  static final int RESULT_POS_NOT_CONF= 96;
  static final int RESULT_POS_WRONG_CONF = 97;
  static final int RESULT_MERCHANT_NOT_RECOGNIZED = 99;
  static final int RESULT_WRONG_ISOFIELDID = 101;
  static final int RESULT_INTERNET_NOT_AVAILABLE = 159;


  private final ReactApplicationContext reactContext;

  private Promise mIngenicoPromise;

  private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {
    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
      if(requestCode == REQUEST_CODE_PAY || requestCode == REQUEST_CODE_REFUND || requestCode == REQUEST_CODE_TTLEXT || requestCode == REQUEST_CODE_CNS ) {
        Log.d(TAG, "Result from activity " + resultCode);
        if(mIngenicoPromise != null && data != null) {
          int status = data.getIntExtra("result", RESULT_UNCAUGHT_EXCEPTION);
          Log.d(TAG, "POS Status " + status);
          Log.d(TAG, "Parsing results");
          if(resultCode == Activity.RESULT_OK && status == RESULT_OK) {
            WritableMap result = null;

            switch (requestCode) {
              case REQUEST_CODE_PAY:
                Log.d(TAG, "Get Results for Payment");
                result = getPaymentResult(data);
                break;
              case REQUEST_CODE_TTLEXT:
                Log.d(TAG, "Get Results for TTL");
                result = getTTLResult(data);
                break;
              case REQUEST_CODE_CNS:
                Log.d(TAG, "Get Results for CNS");
                result = getCNSResult(data);
                break;
              case REQUEST_CODE_REFUND:
                Log.d(TAG, "Get Results for REFUND");
                result = getRefundResult(data);
                break;
            }

            if(result != null) {
              Log.d(TAG, "Resolve promise");
              result.putInt("status", status);
              mIngenicoPromise.resolve(result);
            } else {
              Log.d(TAG, "Rejectpromise");
              Log.e(TAG, getErrorMessage(status));
              mIngenicoPromise.reject(getErrorMessage(status), getErrorMessage(status));
            }
          } else {
            Log.e(TAG, "Error recceiving result ");
            mIngenicoPromise.reject(getErrorMessage(status), getErrorMessage(status));
          }
        } else {
          Log.d(TAG, "Promise not present");
          // if(mIngenicoPromise != null) {
          //   mIngenicoPromise.reject("POS Connection not initialized", new Exception());
          // }
        }
      }
    }
  };


  public RNIngenicoModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
    this.reactContext.addActivityEventListener(mActivityEventListener);
  }

  @Override
  public String getName() {
    return "RNIngenico";
  }

  @ReactMethod
  public void startPayment(
          String amount,
          String reference,
          final Promise promise
          ) {
    Log.d(TAG, "Start Payment");
    Activity currentActivity = getCurrentActivity();

    if(currentActivity == null) {
      promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exists");
    }

    mIngenicoPromise = promise;

    try {
      final Intent intent = new Intent(INGENICO_INTENT_PAYMENT);
      intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

      intent.putExtra(INGENICO_INTENT_PAYMENT + ".amount", amount);
      intent.putExtra(INGENICO_INTENT_PAYMENT + ".field1", reference);
      intent.putExtra(INGENICO_INTENT_PAYMENT + ".field5", "PAY");

      currentActivity.startActivityForResult(intent, REQUEST_CODE_PAY);

    } catch (Exception e) {
      mIngenicoPromise.reject(E_FAILED_TO_SHOW_INGENICO, e);
      mIngenicoPromise = null;
    }
  }

  @ReactMethod
  public void startTTL(
          String amount,
          String reference,
          Promise promise
  ) {
    Activity currentActivity = getCurrentActivity();

    if(currentActivity == null) {
      promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exists");
    }

    mIngenicoPromise = promise;

    try {
      final Intent intent = new Intent(INGENICO_INTENT_TTL);
      final String sheetId = encodeSheetId(reference);
      final String additionalData = INGENICO_TTL_CAUSE + sheetId;
      
      intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

      intent.putExtra(INGENICO_INTENT_TTL + ".amount", amount);
      intent.putExtra(INGENICO_INTENT_TTL + ".cardtype", "23");
      intent.putExtra(INGENICO_INTENT_TTL + ".isofieldin","47");
      intent.putExtra(INGENICO_INTENT_TTL + ".additionaldata", additionalData);

      

      currentActivity.startActivityForResult(intent, REQUEST_CODE_TTLEXT);

    } catch (Exception e) {
      mIngenicoPromise.reject(E_FAILED_TO_SHOW_INGENICO, e);
      mIngenicoPromise = null;
    }
  }

  private String encodeSheetId(String reference) {
    final int length = reference.length();

    char[] chars = reference.toCharArray();

    StringBuffer hex = new StringBuffer();

	  for(int i = 0; i < chars.length; i++){
	    hex.append(Integer.toHexString((int)chars[i]));
	  }

    String encodedId = "DF8102" + String.format("%02X", length) + hex.toString();
    encodedId = encodedId.toUpperCase();
	  
    return encodedId;
  }

  @ReactMethod
  public void startCNS(
          Promise promise
  ) {
    Activity currentActivity = getCurrentActivity();

    if(currentActivity == null) {
      promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exists");
    }

    mIngenicoPromise = promise;

    try {
      final Intent intent = new Intent(INGENICO_INTENT_CNS);
      intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

      currentActivity.startActivityForResult(intent, REQUEST_CODE_CNS);
    } catch (Exception e) {
      mIngenicoPromise.reject(E_FAILED_TO_SHOW_INGENICO, e);
      mIngenicoPromise = null;
    }
  }

  @ReactMethod
  public void startRefund(
          Promise promise
  ) {
    Activity currentActivity = getCurrentActivity();

    if(currentActivity == null) {
      promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exists");
    }

    mIngenicoPromise = promise;

    try {
      final Intent intent = new Intent(INGENICO_INTENT_REFUND);
      intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

      currentActivity.startActivityForResult(intent, REQUEST_CODE_REFUND);
    } catch (Exception e) {
      mIngenicoPromise.reject(E_FAILED_TO_SHOW_INGENICO, e);
      mIngenicoPromise = null;
    }
  }

  private String getNameFromCode(int requestCode) {
    String name = null;
    switch (requestCode) {
      case REQUEST_CODE_PAY:
        name = INGENICO_INTENT_PAYMENT;
        break;
      case REQUEST_CODE_TTLEXT:
        name = INGENICO_INTENT_TTL;
        break;
      case REQUEST_CODE_CNS:
        name = INGENICO_INTENT_CNS;
        break;
      case REQUEST_CODE_REFUND:
        name = INGENICO_INTENT_REFUND;
        break;
    }

    return name;
  }

  private WritableMap getPaymentResult(Intent data) {
    return parseCommonResult(INGENICO_INTENT_PAYMENT, data);
  }

  private WritableMap getTTLResult(Intent data) {
    WritableMap map = parseCommonResult(INGENICO_INTENT_TTL, data);
    map.putString("temporaryCode", null);
    String additionalData = data.getStringExtra(INGENICO_INTENT_TTL + ".additionaldata");
    Log.d(TAG, "TTL Result");
    Log.d(TAG, "Additional data: " + additionalData);

    if(additionalData != null) {
      int index = additionalData.indexOf("DF8D01");



      if(index >= 0) {
        String temporaryCode = additionalData.substring(index + 8, index + 38);

        map.putString("temporaryCode", hexToASCII(temporaryCode));
      }
    }

    return map;
  }

  private WritableMap getCNSResult(Intent data) {
    WritableMap map = Arguments.createMap();
    map.putString("cardType", data.getStringExtra(INGENICO_INTENT_CNS + ".cardtype"));
    map.putString("data", new String(data.getByteArrayExtra(INGENICO_INTENT_CNS + ".data")));
    return map;
  }

  private WritableMap getRefundResult(Intent data) {
    return parseCommonResult(INGENICO_INTENT_REFUND, data);
  }

  private WritableMap parseCommonResult(String action, Intent data) {
    String timestamp;

    WritableMap map = Arguments.createMap();

    timestamp = data.getStringExtra(action + ".timestamp");

    map.putString("date", timestamp);
    map.putString("merchantId", data.getStringExtra(action + ".merchant_id"));
    map.putString("acquirerId", data.getStringExtra(action + ".acquirer_id"));
    map.putString("operationNumber", data.getStringExtra(action + ".operation_number"));
    map.putString("termId", data.getStringExtra(action + ".termid"));
    map.putString("pan", data.getStringExtra(action + ".pan"));
    map.putString("exp", data.getStringExtra(action + ".exp"));
    map.putString("stan", data.getStringExtra(action + ".stan"));
    map.putString("authorizationNumber", data.getStringExtra(action + ".authoritation_number"));
    map.putString("amount", data.getStringExtra(action + ".amount"));
    map.putString("cvm", data.getStringExtra(action + ".cvm"));
    map.putString("additionalData", data.getStringExtra(action + ".additionaldata"));
    map.putString("cardType", data.getStringExtra(action + ".cardtype"));

    return map;
  }

  private String getErrorMessage(int resultCode) {
    String message = null;
    switch (resultCode) {
      case RESULT_INTERNET_NOT_AVAILABLE:
        message = "Internet non disponibile";
        break;
      case RESULT_MERCHANT_NOT_RECOGNIZED:
        message = "Merchant non riconosciuto";
        break;
      case RESULT_PAYMENT_DECLINED:
        message = "Pagamento rifiutato";
        break;
      case RESULT_POS_NOT_CONF:
        message = "POS non configurato";
        break;
      case RESULT_POS_NOT_CONNECTED:
        message = "POS non collegato";
        break;
      case RESULT_POS_NOT_INITIALIZED:
        message = "POS non inizializzato";
        break;
      case RESULT_POS_WRONG_CONF:
        message = "Errore configurazione POS";
        break;
      case RESULT_UNCAUGHT_EXCEPTION:
        message = "POS Errore Sconosciuto";
        break;
      case RESULT_WRONG_ISOFIELDID:
        message = "POS Errore ISO Field";
        break;
      default:
        message = "Errore POS Sconosciuto";
        break;

    }

    return message;
  }

  private static String hexToASCII(String hexValue)
  {
    StringBuilder output = new StringBuilder("");
    for (int i = 0; i < hexValue.length(); i += 2) {
      String str = hexValue.substring(i, i + 2);
      output.append((char) Integer.parseInt(str, 16));
    }
    return output.toString();
  }

}